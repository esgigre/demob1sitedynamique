# Explications pour les fichiers listeVille.php et ficheVille.php

## CRUD 

Pour **C**reate **R**ead **U**pdate **D**elete
Soit les 4 opérations pour lire & mettre a jour

On vas aussi découper en 2 ecrans distincts.

Un écran "liste" qui permet de trouver une entité parmis toutes
Un écran "fiche" qui permet d'afficher le détail d'une entitée et de la modifier

## ListeVille.php

![Listeville.php](./images/ListeVille.png)

Comme son nom laisse à pensser : Cette page affiche **toutes** les villes dans un tableau (plus un outil de recherche)

Le fonctionnement 'global' du programme c'est : 

- Fait une requete SQL qui charge toutes les villes
- Affiche le début d'un tableau HTML
- Pour chaque ligne de la requete, génére une ligne de tableau
- Affiche la fin du tableau
- Appelle le script datatables qui repeint le tableau en joli

### Lecture commentée

Ligne 1 à 5 : Du HTML "en dur"
```html
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Liste des villes</title>
```

Ligne 6 : Appel du CSS de Bootstrap
```html
        <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
```

Ligne 7 : Appel de *mon* CSS
Aprés
pour etre prioritaire sur Bootstrap
```html
        <link href="../css/style.css" rel="stylesheet">
```

Ligne 8 à 11 : Du HTML "en dur"
```html
    </head>
    <body>
        <div class="container">
            <h1>Les villes</h1>
```

Ligne 12 : On commence le PHP
```php
<?php
```

Ligne 13 : Appel du script connexion.php qui ... connecte à la base de donnée

Je récupére la variable $bdd qui sera mon point d'entrée pour "faire du SQL"

```php
include("../include/connexion.php");
```

