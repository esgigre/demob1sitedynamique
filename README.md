# TP 1er année : Réalisation d'un site web dynamique

## Prérequis

Notions de :
*  html
*  css
*  php
*  sql
*  ligne de commande (Windows, Mac ou Linux)

Vagues notions de : 
*  javascript

## Trucs à installer sur le PC

### Un bon IDE

Bien : [VSCode](https://code.visualstudio.com/)

Mieux : [Codium](https://vscodium.com/)

## xAMP

**A** pour Apache
**M** pour MariaDB
**P** pour PHP
**x** pour Windows, Mac ou Linux

Pour Windows & Mac : [xampp](https://www.apachefriends.org/download.html) 
Prenez la derniére version

Pour Linux : 
```bash
sudo apt install apache2 php php-cli php-{curl,gd,intl,memcache,xml,zip,mbstring,json} libapache2-mod-php php-mysql mariadb-server
```

## Git

Pour Windows : [git](https://git-scm.com/download/windows) prenez le "Standalone installer 64 bits"

Pour Mac : [git](https://git-scm.com/download/mac) 

Pour Linux : 
```bash
sudo apt install git-all
```

**Pour les 3 OS** une fois Git installé passer les commandes suivantes :
```bash
git config --global user.email "<votre mail>"
git config --global user.name "<votre pseudo>"
```

⚠️ Le mail doit etre le même que celui du compte gitlab

## Etape 1 : dépot Git

But : Connecter un dossier local de votre PC à un serveur Git

### Des clé SSH

Pour les 3 OS : 

⚠️ Pour windows utiliser `PowerShell`, pas `cmd`

```bash
ssh-keygen -t ed25519
```

Répondre <entrée> au 3 questions

Vous vennez de générer une paire de clé (clé privée + clé publique) sur votre PC

Elles s'apellent :
*  `id_ed25519` => Clé privée : Ne jamais donner
*  `id_ed25519.pub` => Clé publique : A distribuer au monde entier

Elles sont dans : 
*  Windows => `C:\Users\<user>\.ssh\`
*  Linux & Mac => `/home/<user>/.ssh/`

### Un serveur Git

Créez un compte gratuit chez 
Bien : [GitHub](https://github.com/)
Mieux : [GitLab](https://gitlab.com/)

⚠️ Utilisez le même mail que pour l'installation git

### Ajouter sa clé SSH

#### **Copiez votre clé publique**

Pour afficher le contenu de votre clé publique

Mac & Linux
```bash
# Allez dans le dossier ou elle se trouve
cd ~/.ssh
# Afficher le contenu
cat id_ed25519.pub
```

Windows
```bash
# Allez dans le dossier ou elle se trouve
cd ~\.ssh
# Afficher le contenu
type id_ed25519.pub
```

Copiez **toute** la ligne zarbi

#### **Collez votre clé publique**

Pour Gitlab : 
* Cliquez sur votre profil (en haut à droite)
* Cliquez sur `edit profile`
* Cliquez sur `ssh keys` (colonne de gauche)
* Collez votre clé dans le pavé `key` **sans la modifier**
* Vous pouvez changer le champ `title`
* Puis bouton `add key`

Pour GitHub : 
* Démerdez-vous (je vous avait dit de prendre GitLab 😈)

### Nouveau projet

* Revenez sur la page d'acceuil de Gitlab
* Cliquez sur `New Project`
* Prendre "Create blank project"
* Remplir le Project Name : Site Web Dynamique (par exemple)
* Noter le `Project slug` (ce sera le nom du dossier sur le PC)
* Verifier que `Initialize repository with a README` est coché
* Cliquez `Create Project`
  
### Cloner sur le PC

![Cloner sur le PC](./images/gitlab.clone.png)

* Sur la fiche de votre projet sur GitLab : 
  * Dépliez le bouton `Clone`
  * Copiez le lien `clone with SSH`

* Sur votre PC, en ligne de commande

Pour Linux & Mac
```bash
# Aller dans un dossier connu par Apache
cd /var/www
# Clonner le projet
git clone <coller ici>
```

Pour Windows
```bash
# Aller dans un dossier connu par Apache
cd c:\xampp\htdocs
# Clonner le projet
git clone <coller ici>
```

### Travailler avec l'IDE

**IDE** = Integrated Development Environment
En clair des trucs comme VSCode; Codium; Atom; PHPStorm; Eclipse ...

#### **Ouvrir le projet** 

Ouvrez le dossier que vous venez de clonner dans Codium :

(Depuis l'explorateur de fichiers, clic droit + Ouvrir avec)

#### **Faire une modifiaction**

Changer le contenu du fichier README.md

Le README.md est le fichier affiché par défaut sur la page gitlab de votre projet

Son contenu actuel est un truc auto généré dont on se fiche !

Remplacez tout par : 
```md
# Mon site web dynamique
... a compléter
```

### Remonter la modifiaction sur le serveur

Une fois le fichier sauvegardé, une notification apparait sur la colonne de gauche sur l'icone Git

Il y a 3 étapes à comprendre

Chaqune peut etre réalisé en ligne de commande ... ou d'un clic dans l'IDE

#### **Git add**
=> Dire à git quels fichiers sont concernés.

En ligne de commande il faut les nomer un par un avec `git add fichier.txt`
ou tous d'un coup avec `git add .`

Avec Codium il faut cliquer sur le **+** à droite du fichier (<span style="color:white; background-color:red; border-radius:5px; width=10px">1</span>)

![Git add & commit](./images/ide.commit.png)

#### **Git commit**
=> Donner un nom au lot de modification

Cette étape est importante pour vous aider, vous même, plus tard pour retrouver une modification précise

![Git commit](./images/git.commit.png)
[Un court manuel pour bien nomer ses commits](https://buzut.net/cours/versioning-avec-git/bien-nommer-ses-commits)

Pour Codium c'est là : <span style="color:white; background-color:red; border-radius:5px; width=10px">2</span> + <span style="color:white; background-color:red; border-radius:5px; width=10px">3</span>

En ligne de commande c'est `git commit -m "texte du commit"`

#### **Git push**
=> Envoyer le commit sur le serveur

![Git push](./images/ide.push.png)

Pour codium : <span style="color:white; background-color:red; border-radius:5px; width=10px">1</span>

En ligne de commande : `git push`


**Verifiez que votre nouveau README est bien sur le site gitlab** (pensez à rafraichir la page)

## NPM

NPM est "le" gestionaire de paquer JavaScript. le truc qu'on vas utiliser pour installer proprement plien de plugins qui vont faciliter notre développement.

NPM est une partie du framework NodeJS (qui marche en JavaScript)

Donc pour l'installer ... on vas installer NodeJS

### Linux 

Source : https://deb.nodesource.com/

```bash
sudo apt-get update && sudo apt-get install -y ca-certificates curl gnupg
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
NODE_MAJOR=20
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list
sudo apt-get update && sudo apt-get install nodejs -y
```

### Windows

[Installateur msi Node V20.11 64 bits](https://nodejs.org/dist/v20.11.1/node-v20.11.1-x64.msi)

### Mac

[Installateur pkg Node V20.11 pour Mac / Intel](https://nodejs.org/dist/v20.11.1/node-v20.11.1.pkg)

## Ajouter des trucs avec npm

### Exemple : Bootstrap

Bootstrap c'est une série de feuilles CSS et quelques outil en JS qui aident grandement à faire une mis en page qui pếte des culs (terme technique pour dire que c'est bigement bien)

En ligne de commande, depuis le dossier racine du projet

```bash
npm install bootstrap
```

Ca télécharge bootstrap dans `nodes_modules/bootstrap` 

Et ca ecrit dans le fichier package.json la version requise
```
    "bootstrap": "^5.3.3",
```

le `^` veut dire `au moins`

### Installer toutes les dépendances

En ligne de commande, depuis le dossier racine du projet

```bash
npm install
```

Ca installera / mettra à jour toutes les dépendanses listées dans `package.json`

## .gitignore

Il existe à la racine du projet un fichier .gitignore que vous devez remplir avec la liste des fichiers / dossiers a **ne pas** mettre sur le serveur git (qui est public)

On vas mettre le dosier /nodes_modules qui ne contient que du code téléchargé (donc pas ecrit par nous)
et surtout le fichier .env qui contient le mot de passe de la base SQL (et le user & le nom de la base)
```
.env
node_modules/
```

Le fichier .gitignore lui même doit etre commité & poussé sur le serveur

## Etape 2 : Base de donnée

But : Installer la base en local

### PhpMyAdmin

Windows & Mac : 
Verifier que sur xampp Apache & MariaDB soient bien démarés

Linux : 
C'est tout bon 😉 

Pour y acceder : 

[PhpMyAdmin](http://localhost/phpmyadmin)

### Créer une base

![Git push](./images/PMA.create.png)

### Créer un utilisateur dédié

```sql
Create user `TPPHP`@`localhost` identified by 'TPPHP';
Grant all privileges on TPPHP.* to `TPPHP`@`localhost`;
```

### Peupler la base

Importez le fichier `TPPHP.sql` dans la base `TPPHP`

...

Vous obtenez ça : 

![Git push](./images/PMA.MCD.png)

### Noter les identifiants

On vas créer un fichier `.env` à la racine du projet qui vas contenir les infos pour que le code PHP puisse utiliser la base MySQL


```bash
# Identifiants de connexion à la base
host=localhost
database=TPPHP
user=TPPHP
password=TPPHP 
```
💀 : Le `.env` **doit** etre dans le `.gitignore`
Si vous le commitez sur le serveur : [Vous venez de vous chocoblaster vous même !](https://www.chocoblast.fr/reglement/)

## Les dossiers du projet

### CSS

On met les feuilles de style ici

### DB

Un dossier "poubelle" ou on met le dump de la base de donnée
Il ne sera pas accesible depuis le web

### Images

Toutes les images incluses dans le site web (ou appelées depuis ce document) sont ici

On peut créer, si besoin, des sous dossiers

### Include

On met ici des scripts PHP "techniques", des classes, des bibliothéques de fonctions ... 
comme par exemple

##### connexion.php

C'est le code PHP qui établit la connexion à la base de donnée
Il faut - juste - l'inclure dans chaque page PHP qui 'fait' du SQL

```php
<?php
include("../include/connexion.php");
```

#### node_modules

Tout le bordel téléchargé par `npm install`
Ne doit pas etre commité sur le serveur
Ne doit pas etre modifié

C'est du code géré par npm, si on le modifie, il sera écrasé à la prochaine mise à jour

Pour 'activer' un module NPM il faut lire la doc pour voir quel fichier appeler

Ex pour Bootstrap : 

```html
<link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
```

#### Public

C'est ici qu'on met les pages du site web.
Il faut notement un fichier `index.php` ou `index.html` qui est le ficher appelé par défaut (= page d'accueil du site)